class NameType:

    def __init__(self, label):
        assert type(label) is str
        self.label = label
        self.maxid = 0
        self.all_names = []
        self.reset_maxid = 0  # used only when reseting all the names of a type
        self.old2new_name_mapping = {}

    def __repr__(self):
        sout = '===namestype==\n'
        return(sout)

    def __repr__(self):
        return('<namespace: >')

    def create_name(self):
        l = self.label + '_' + ('%d' % self.maxid)
        self.maxid += 1
        while(l in self.all_names):
            self.maxid += 1
            l = self.label + '_' + ('%d' % self.maxid)
        self.all_names.append(l)
        return(l)

    def add_name(self, nm):
        self.all_names.append(nm)

    def rename(self, old_nm, new_nm):
        self.all_names[self.all_names.index(old_nm)] = new_nm

    def create_replacement_name(self):
        l = self.label + ('%d' % self.reset_maxid)
        self.reset_maxid += 1
        assert(l not in self.old2new_name_mapping.keys())
        return(l)

    def reset_names(self):
        self.old2new_name_mapping = {}
        self.reset_maxid = 0
        for n in self.all_names:
            new_n = self.create_replacement_name()
            self.all_names[self.all_names.index(n)] = new_n
            self.old2new_name_mapping[n] = new_n
            self.reset_maxid += 1
        self.maxid = self.reset_maxid
        return(self.old2new_name_mapping)


class NameSpace :

    def __init__(self, typelist=[]):
        self.all_types = {}
        for t in typelist:
            self.add_type(t)

    def __str__(self):
        sout = '===namespace==\n'
        sout += '====='
        return(sout)

    def __repr__(self):
        return('<namespace: >')

    def add_type(self, typ):
        if typ not in self.all_types:
            self.all_types[typ] = NameType(typ)

    def remove_type(self, typ):
        del self.all_types[typ]

    def create_name(self, typ):
        return(self.all_types[typ].create_name())

    def which_name_type(self, nm, typ=None):
        # can be used also as an existence predicate
        if(typ is None):
            for t in self.all_types.keys():
                if(nm in self.all_types[t].all_names):
                    return(t)
            return(None)
        else:
            if(nm in self.all_types[typ].all_names):
                return(typ)
            else:
                return None

    def add_name(self, nm, typ=None):
        t = self.which_name_type(nm)
        if(t in self.all_types.keys()):
            print('ERROR the name {} with name type {} cannot be added because it already exists!'.format(
                nm, typ))
            assert(0)
        else:
            assert(typ in self.all_types.keys())
            self.all_types[typ].add_name(nm)

    def rename(self, old_nm, new_nm, typ=None):
        if(typ is None):
            for t in self.all_types.keys():
                if(old_nm in self.all_types[t].all_names):
                    self.all_types[t].rename(old_nm, new_nm)
        else:
            if(self.which_name_type(old_nm, typ) is not None):
                self.all_types[typ].rename(old_nm, new_nm)

    def remove_name(self, nm, typ=None):
        if(typ is None):
            for t in self.all_types.keys():
                if(nm in self.all_types[t].all_names):
                    self.all_types[t].all_names.remove(nm)
        else:
            if(nm in self.all_types[typ].all_names):
                self.all_types[typ].all_names.remove(nm)

    def reset_names(self, typ=None):
        if(typ is None):
            self.name_mapping = {}
            for t in self.all_types.keys():
                m = self.all_types[t].reset_names()
                self.name_mapping = {**self.name_mapping, **m}
            return(self.name_mapping)
        else:
            if(typ in self.all_types):
                return(self.all_types[typ].reset_names())

if __name__ == "__main__" :
    s = NameSpace(typelist=["type1", "type2"])
    print(s)
