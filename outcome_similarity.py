from transformers import AutoTokenizer, AutoModel
from sentence_transformers import util
import torch
import torch.nn.functional as F
from typing import List
from pandas import DataFrame


class OutcomeSimilarity:
    """ similarity detector between outcomes statements"""
    ID2LABEL = ["different", "similar"]

    def __init__(self, model_path: str):
        self.tokenizer = AutoTokenizer.from_pretrained(model_path)
        self.model = AutoModel.from_pretrained(model_path)

    # Mean Pooling - Take attention mask into account for correct averaging
    def mean_pooling(self, model_output, attention_mask):
        # First element of model_output contains all token embeddings
        token_embeddings = model_output[0]
        input_mask_expanded = attention_mask.unsqueeze(
            -1).expand(token_embeddings.size()).float()
        return torch.sum(token_embeddings * input_mask_expanded, 1) / torch.clamp(input_mask_expanded.sum(1), min=1e-9)

    def encode(self, sentences: List[str]):
        # Tokenize sentences
        encoded_input = self.tokenizer(
            sentences, padding=True, truncation=True, return_tensors='pt')
        # Compute token embeddings
        with torch.no_grad():
            model_output = self.model(**encoded_input)
        # Perform pooling
        sentence_embeddings = self.mean_pooling(
            model_output, encoded_input['attention_mask'])
        # Normalize embeddings
        sentence_embeddings = F.normalize(sentence_embeddings, p=2, dim=1)
        return sentence_embeddings

    def encode_outcome_dict(self, outcome_dict):
        types, embs, sents = [], [], []
        for outcome_type, sentences in outcome_dict.items():
            for s in sentences:
                types.append(outcome_type)
                embs.append(self.encode(s))
                sents.append(s)
        return types, embs, sents

    def get_similarity(self, true_dict, compared_dict):
        rows = []
        compared_types, compared_embs, compared_sents = self.encode_outcome_dict(
            compared_dict)
        true_types, true_embs, true_sents = self.encode_outcome_dict(true_dict)

        for i in range(len(true_types)):
            if true_types[i] not in ["other", "id"]:
                best_cosine = -1
                best_ind = -1
                for j in range(len(compared_types)):
                    cosine = util.cos_sim(
                        true_embs[i], compared_embs[j]).item()
                    if cosine >= best_cosine:
                        best_cosine = cosine
                        best_ind = j
                predicted_label = 0 if best_cosine < 0.44 else 1
                row_result = {"Registry Outcome": true_sents[i],
                              "Registry Type": true_types[i],
                              "Article Most Similar": compared_sents[best_ind],
                              "Article Type": compared_types[best_ind],
                              "Similarity Score": best_cosine,
                              "Is Similar": predicted_label}
                rows.append(row_result)
        return DataFrame(rows)
