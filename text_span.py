from __future__ import annotations # for being able to use annotations of later defined classes
from typing import List, Tuple, Dict
import codecs


class MultiWordUnit:
    # --- class variables
    DEFAULT_NAME_PREFIX = 'MWU'
    DEFAULT_COUNT = 0
    # text_spans is a list of pairs of character offsets e.g.  MultiWordUnit(name='MWU_4', text_spans=[(13300 , 13320)])
    # example of MultiWordUnit structure:
    # m1=mwu( nm='MWU_4', txtsps=[ (13300 , 13320)], annots={'comments':'ceci est un test', 'POS_GRACE':'Ncms'})

    def __init__(self, name : str=None, text_spans: List[Tuple[int,int]]=[], type: str='', annotations:Dict[str,str]={}) -> None:
        assert type(text_spans) is list
        # --- name.
        if name:
            self.name = name
        else:
            self.name = self.DEFAULT_NAME_PREFIX + \
                '_' + str(self.DEFAULT_COUNT)
            self.DEFAULT_COUNT += 1
        self.text_spans = text_spans
        # --- annotations global to the MultiWordUnit (attribute value associations in a dictionary).
        self.annotations = annotations
        self.type = type

    def text_n(self, document: Document, n: int) -> str:
        tsp = self.text_spans[n]
        assert type(tsp) is tuple
        return document.span(tsp[0], tsp[1])

    def text(self, document: Document, sep: str = ''):
        return sep.join([self.text_n(document,i) for i in range(len(self.text_spans))])

    def span_n(self, n:int):
        if(n < len(self.text_spans)):
            return(self.text_spans[n])
        else:
            return(None)

    def __repr__(self) -> str:
        res = 'mwu( nm=' + self.name.__repr__() + ', txtsps=['
        n = 0
        for sp in self.text_spans:
            res += sp.__repr__()
            n += 1
            if(n < len(self.text_spans)):
                res += ', '
        res += '])'
        return res

    def __str__(self) -> str:
        res = '---mwu:\n\tname= ' + str(self.name)
        res += '\n\ttextspans is: ' + str(self.text_spans)
        res += '\n\tannotations is: ' + str(self.annotations)
        res += '\n'
        return res

    def size(self) -> int:
        return len(self.text_spans)

    # ---- annotations of the whole MultiWordUnit)
    def set_annot(self, aspect, value):
        self.annotations[aspect] = value

    def get_annot(self, aspect):
        return(self.annotations[aspect])


class Relation:
    def __init__(self, name='', source=None, target=None, tp=None):
        assert type(name) is str
        self.name = name  # a string identifier for the MultiWordUnitRelation
        self.src = source  # an identifier in all_span data member of ConstruKT instance
        self.trgt = target  # an identifier in all_span data member of ConstruKT instance
        self.typ = tp

    def __repr__(self):
        res = 'rel( nm=' + self.name.__repr__() + ', source=' + self.src.__repr__() + \
            ', target=' + self.trgt.__repr__() + ', tp=' + self.typ.__repr__() + ')'
        return(res)

    def __str__(self):
        sout = 'rel: name= ' + str(self.name) + ' typ= ' + str(self.typ) + \
            ' src= ' + str(self.src) + ' trgt= ' + str(self.trgt) + '\n'
        return(sout)


class MultiWordUnitRelation(Relation):

    def __init__(self, nm='', src=None, trg=None, annotations={}, typ=None):
        assert type(nm) is str
        assert type(src) is MultiWordUnit
        assert type(trg) is MultiWordUnit
        super.__init__(self, nm, src.name, trg.name, typ)
        self.source = src
        self.target = trg
        self.annotations = annotations

    # ---- annotations of the whole MultiWordUnit)
    def set_annot(self, aspect, value):
        self.annotations[aspect] = value

    def get_annot(self, aspect):
        return(self.annotations[aspect])


class Construction:
    def __init__(self, nm=None, rel_lst=[]):
        assert type(nm) is str
        self.name = nm
        self.rels = rel_lst

    def __repr__(self):
        sout = 'construction( nm=' + self.name.__repr__() + ', rels=['
        n = 0
        for r in self.rels:
            sout += r.__repr__()
            n += 1
            if(n < len(self.rels)):
                sout += ', '
        sout += ']'
        return sout

    def __str__(self):
        sout = 'construction: name= ' + str(self.name) + 'rels='
        for r in self.rels:
            sout += r.__str__()
        sout += '\n'
        return sout

# --- end of class construction


class Document:

    def __init__(self, id=None, content='', metadata='', multi_word_units=[], MultiWordUnitRelations=[], constructions=[]):
        if type(id) is str and id:
            self.id = id
        else:
            raise TypeError("id must be a non-empty string")
        if type(content) is str:
            self.content = content
        else:
            raise TypeError("content must be a string")
        # potentialy a document has multi-word units (mwus) annotations, MultiWordUnitRelations (between two MultiWordUnits, a source and a target) and constructions (sets of MultiWordUnitRelations)
        self.mwus = multi_word_units
        self.rels = MultiWordUnitRelations
        self.kstructs = constructions

    def len(self):
        return(len(self.content))

    def byte_len(self):
        return(len(self.content.encode('utf8')))

    def get_content(self):
        return(self.content)

    def span(self, first, last):
        return(self.content[first:last])

    def __repr__(self):
        res = 'document( ident=\'' + self.id + '\''
        res += '\t\n, content=' + self.content.__repr__()
        res += '\t\n, metadata=' + self.meta.__repr__()
        res += '\t\n, multi_word_units = ['
        i = 0
        for m in self.mwus:
            if(i > 0):
                res += ', '
            i += 1
            res += m.__repr__()
        res += ']\n'
        res += '\t, MultiWordUnitRelations= ['
        i = 0
        for r in self.rels:
            if(i > 0):
                res += ', '
            i += 1
            res += r.__repr__()
        res += ']\n'
        res += '\t, constructions= ['
        i = 0
        for k in self.kstructs:
            if(i > 0):
                res += ', '
            i += 1
            res += k.__repr__()
        res += '] )\n'
        return(res)

    def __str__(self):
        res = 'document is:' + '\tid= ' + str(self.id)
        res += '\tmeta= ' + str(self.meta)
        res += '\tlen(self.content)= ' + str(len(self.content))
        res += '\tlen(mwus)=' + str(len(self.mwus))
        res += '\tlen(rels)=' + str(len(self.rels))
        return(res)

    # ------- file io

    def read(self, path):
        assert((type(path) is str) and (path != ''))
        buffer_sz = 100
        in_strm = codecs.open(
            path, mode='r', encoding='utf-8', errors='strict', buffering=-1)
        assert(in_strm)
        in_data = u''
        for l in in_strm.readlines(buffer_sz):
            in_data += l
        self.content += in_data
        in_strm.close()

    def write(self, path):
        assert((type(path) is str) and (path != ''))
        out_strm = codecs.open(
            path, mode='w', encoding='utf-8', errors='strict', buffering=-1)
        assert(out_strm)
        out_strm.write(self.content)
        out_strm.close()

    #--- MultiWordUnits

    def set_mwu(self, m=[]):
        assert(type(m) is list)
        for e in m:
            assert(type(e) is MultiWordUnit)
        self.mwus = m

    def add_mwu(self, m):
        assert((type(m) is MultiWordUnit) or (type(m) is list))
        if(type(m) is list):
            self.mwus.extend(m)
        else:
            self.mwus += [m]

    def remove_mwu(self, m):
        assert((type(m) is MultiWordUnit) or (type(m) is list))
        if(type(m) is list):
            for e in m:
                self.mwus.remove(e)
        else:
            self.mwus.remove(m)

    def get_mwus(self):
        return(self.mwus)

    def find_mwu_containing_span(self, s):
        assert((type(s) is tuple) and (len(s) == 2) and (
            type(s[0]) is int) and (type(s[1]) is int) and (s[0] < s[1]))
        for m in self.mwus:
            if(m.contains_span_p(s)):
                return(m)
        return(None)

    #--- MultiWordUnitRelations

    def set_rel(self, r=[]):
        assert(type(r) is list)
        for e in r:
            assert(type(e) is MultiWordUnitRelation)
        self.rels = r

    def add_rel(self, r):
        assert((type(r) is MultiWordUnitRelation) or (type(r) is list))
        if type(r) is list:
            self.rels.extend(r)
        else:
            self.rels += [r]

    def remove_rel(self, r):
        assert((type(r) is MultiWordUnitRelation) or (type(r) is list))
        if(type(r is list)):
            for e in r:
                self.rels.remove(e)
        else:
            self.rels.remove(r)

    def get_rels(self):
        return(self.rels)
