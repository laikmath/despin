from transformers import pipeline
from typing import List, Dict, Tuple, Any


class OutcomeExtractor:

    def __init__(self, model_path: str):
        self.generator = pipeline(
            "ner", 
            model=model_path, 
            tokenizer=model_path, 
            ignore_labels=[],
            aggregation_strategy="simple")

    def get_text_batchs(self, text: str, batch_size=256):
        batch_token_list = []
        count = 0
        batch = []
        tokens = text.lower().split()
        if len(tokens) > batch_size:
            for token in tokens:
                batch.append(token)
                if count > batch_size:
                    batch_token_list.append(' '.join(batch))
                    batch = []
                    count = 0
                count += 1
        else:
            batch_token_list = [' '.join(tokens)]
        return batch_token_list


if __name__ == "__main__" :
    with open("./txt_examples/long.txt",'r') as f:
        test_text = f.read()
    model = "Mathking/pubmedbert-abs_pri-sec_out"
    oex = OutcomeExtractor(model)
    ht_output = oex.extract_ht_output(test_text)