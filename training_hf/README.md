# Replication experiences of DeSpin 

Installation of required Python librairies:
```bash
pip install -r requirements.txt
```

Must download datasets from [this zenodo page](https://zenodo.org/communities/miror/search?page=1&size=20&q=Koroleva&type=dataset)

## Outcomes token classification

In the folder `outcomes_ner` we replicate the outcome extraction experiences, for primary and reported outcome separately.

1. [`convert_to_hf_dataset.py`](./outcomes_ner/convert_to_hf_dataset.py) file is used to convert the original tsv files to the HuggingFace `datasets` format.
2. [`data_exploration.ipynb`](./outcomes_ner/data_exploration.ipynb) : used to check some caracteristics of the datasets
3. [`config_generation.py`](./outcomes_ner/config_generation.py) : used to generate the different configurations we want to test for training
4. [`training_single_split.py`](./outcomes_ner/training_single_split.py) is the main training file that we use to train different transformers models from HuggingFace using the Trainer API. The test set results are also stored in 3 files (for each split) : `metrics.json` (that stores f1-score, precision, recall and accuracy according to seqeval evaluation), `predictions.npy` (that stores the predictions logits in a numpy array format) and `config.json` (that stores the training configuration). The evaluation is also made on the test set at the end of training.
5. [`update_results.py`](./outcomes_ner/update_results.py) is the file which is used for updating the `results.csv` file where all the metrics are stored for each experience. It also creates the mean results (averaging the results of multiple same experiences where only the random initialization differs)
6. [`error_analysis.ipynb`](./outcomes_ner/error_analysis.ipynb) is a notebook where we analyze where the model fails

The results are reported in Markdown and TSV format in the [`global_results`](./outcomes_ner/global_results) folder.

## Outcomes semantic similarity

In the folder `outcomes_similarity`we replicate the outcome semantic similarity experiences. The file are basically the same as before, but for the task of Text Classification ( = Sequence Classification), so the evaluation computation is different but we still report the same metrics.

* we added some experiences using [`sentence_transformers library`](https://www.sbert.net/) in the  as they proved to be efficient for the task of semantic similarity  
    * `train_single_st.py` file : this script trains a single split to be able to parralelize the 10 fold training. The training configurations are stored in `st_configs` directory. Command to run a training on a particular configuration :  `python3 single_train_st.py --config <config_path> `. The evaluation is automatically done and stored in a csv file at the end of training.
    * `st_evaluation.py` uses the evaluation files from the splits to do the mean evaluation.
* in `unsup_st_evaluation.py` we ran an experiment with sentence transformers in an unsupervised setting (only optimising the cosine similarity threshold).
