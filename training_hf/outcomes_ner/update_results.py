import os
import json
import pandas as pd
from argparse import ArgumentParser

def get_test_results(test_dir) -> pd.DataFrame:
    result_list = []
    for subdir in os.listdir(test_dir):
        res_dict = {}
        res_dict["model"],_, res_dict["dataset"] = subdir.split("_")
        with open(os.path.join(test_dir, subdir, "metrics.json"), "r") as f:
            model_result = json.load(f)
        for k, v in model_result.items():
            if "test_overall" in k:
                res_dict[k.split("_")[-1]] = v
        with open(os.path.join(test_dir, subdir, "config.json"), "r") as f:
            config = json.load(f)
        for k, v in config.items():
            if k not in ["train_out_dir", "test_out_dir", "dataset_path", "model_name"]:
                res_dict[k] = v
        result_list.append(res_dict)
    return pd.DataFrame(result_list)


def combine_dataframes(new_df, last_df_csv_path):
    last_df = pd.read_csv(last_df_csv_path, index_col=0)
    concat_df = pd.concat([last_df, new_df], axis=0)
    subset = ["model","dataset","max_seq_length","batch_size","learning_rate","warmup_ratio","num_train_epochs","seed","patience"]
    concat_df.drop_duplicates(inplace=True, keep="first", ignore_index=True, subset=subset)
    return concat_df


def update_results(test_dir, results_file):
    new_df = get_test_results(test_dir)
    if os.path.isfile(results_file):
        result_df = combine_dataframes(new_df, results_file)
    else:
        result_df = new_df
    result_df.sort_values(by="f1", ascending=False, ignore_index=True, inplace=True)
    result_df.to_csv(results_file)


def write_mean_results(results_file : str) -> None :
    df = pd.read_csv(results_file, index_col=0)
    mean_df = (df.groupby(["model","dataset","max_seq_length","batch_size","learning_rate","warmup_ratio","num_train_epochs","patience"])
      .agg({"f1":"mean","precision":"mean","recall":"mean","accuracy":"mean","seed":"count"})
      .reset_index()
      .rename(columns={"seed":"model_seed_nb"})
      .sort_values(by="f1", ascending=False, ignore_index=True)
    )
    mean_df.to_csv("mean_df.csv")

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('results_file', nargs='?', default="results.csv")
    args = parser.parse_args()

    test_dir = "out/test"
    results_file = args.results_file
    update_results(test_dir, results_file)
    write_mean_results(results_file)