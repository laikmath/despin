from sentence_transformers import SentenceTransformer, evaluation
from datasets import load_from_disk
from os.path import dirname, realpath, join


class STUnsupEvaluator:

    def __init__(self, model_name:str):
        self.model_name = model_name
        self.model = SentenceTransformer(model_name)
        self.filepath = dirname(realpath(__file__))
    
    def evaluate_dataset(self, data_path:str, split:int):
        devset_path = join(data_path,str(split),"dev")
        dataset = load_from_disk(devset_path)
        evaluator = evaluation.BinaryClassificationEvaluator(
            sentences1=dataset["text"],
            sentences2=dataset["text_pair"],
            labels=dataset["label"],
            batch_size=32,
        )
        self.model.evaluate(
            evaluator,
            join(self.filepath,"test_results",self.model_name + f"_{str(split)}")
        )

if __name__ == "__main__" : 
    # Vars
    DATA_PATH = "/mnt/beegfs/home/laiking/projets/replication/outcomes_similarity/data"
    MODEL_NAME = "all-mpnet-base-v2"
    # Run evaluation
    evaluator = STUnsupEvaluator(MODEL_NAME)

    for split in range(10):
        evaluator.evaluate_dataset(DATA_PATH, split)