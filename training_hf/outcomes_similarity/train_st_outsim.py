"""
    Sentence Transformers Trainer for a single split for outcome similarity task
    use :
    python train_st_outsim.py --config <config_path> --split <split_nb>
"""
from sentence_transformers import SentenceTransformer, InputExample, LoggingHandler
from sentence_transformers.losses import CosineSimilarityLoss
from sentence_transformers.evaluation import BinaryClassificationEvaluator
from datasets import load_from_disk, concatenate_datasets
from torch.utils.data import DataLoader
from argparse import ArgumentParser
from os.path import join
from math import ceil
import logging
import json


class STOutcomeSplitTrainer:
    """Sentence Transformers Outcome Similarity Trainer
    for a single dataset split"""
    def __init__(self, config_path:str, split:str) -> None :
        with open(config_path,'r') as f :
            config = json.load(f)
        self.config = config
        self.split = split
        self.model_save_path = join("./out/",self.config["model_name"] + "_" + self.split)
 
        
    def load_train_set(self, dataset_path:str="", train_all_data=False):
        logging.info("Read Outcome Sim train dataset")
        # Loading Dataset
        if not dataset_path :
            dataset_path = join(self.config["dataset_path"],self.split)
        dataset = load_from_disk(dataset_path)
        # Defining training set
        if train_all_data:
            training_set = concatenate_datasets([dataset["train"],dataset["dev"]])
        else :
            training_set = dataset["train"]
        # training set
        train_examples = [InputExample(
                    guid = str(example['sent_id']),
                    texts = [example["text"], example["text_pair"]],
                    label = float(example["label"])
                ) for example in training_set]
        logging.info("Train samples: {}".format(len(train_examples)))
        self.train_dataloader = DataLoader(train_examples, shuffle=True, batch_size = self.config["batch_size"])
    
    def load_dev_set(self,dataset_path:str=""):
        logging.info("Read Outcome Sim dev dataset ")
        # Loading Dataset
        if not dataset_path :
            dataset_path = join(self.config["dataset_path"],self.split,"dev")
        dataset = load_from_disk(dataset_path)
        # dev set
        self.evaluator = BinaryClassificationEvaluator(
            sentences1=dataset["text"],
            sentences2=dataset["text_pair"],
            labels=dataset["label"],
            batch_size=self.config["batch_size"],
        )
    
    def load_model(self, model_name:str=""):
        if not model_name:
            model_name = self.config["model_name"]
        self.model = SentenceTransformer(model_name)

    def train(self, loss='cosine'):
        self.load_model()
        if loss=="cosine":
            train_loss = CosineSimilarityLoss(self.model)
        train_steps = len(self.train_dataloader)
        warmup_steps = ceil(train_steps * self.config["epochs"] * self.config["warmup_ratio"])
        logging.info("Warmup-steps: {}".format(warmup_steps))
        self.model.fit(
            train_objectives=[(self.train_dataloader,train_loss)],
            evaluator=self.evaluator,
            epochs = self.config["epochs"],
            evaluation_steps=int(train_steps*self.config["evaluation_steps_ratio"]),
            warmup_steps = warmup_steps,
            output_path = self.model_save_path,
            show_progress_bar=False
        )
    
    def test_model(self):
        model = SentenceTransformer(self.model_save_path)
        self.evaluator(model, self.model_save_path)
    

if __name__ == "__main__" :
    parser = ArgumentParser()
    parser.add_argument("--config", required=True, type=str)
    parser.add_argument("--split", required=True, type=str)
    args = parser.parse_args()

    logging.basicConfig(format='%(asctime)s - %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S',
            level=logging.INFO,
            handlers=[LoggingHandler()])

    stt = STOutcomeSplitTrainer(args.config, args.split)
    # stt.load_train_set(train_all_data=False)
    stt.load_dev_set()
    # stt.train()
    stt.test_model()
    