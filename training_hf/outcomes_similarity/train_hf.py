from datasets import load_from_disk, load_metric, disable_progress_bar
from transformers import Trainer, TrainingArguments, AutoModelForSequenceClassification, AutoTokenizer, DataCollatorWithPadding, EarlyStoppingCallback
from os import sep, makedirs
from os.path import join
import numpy as np
import json
import argparse
import wandb

class PairClassifier:
    """HuggingFace Pair Classifier"""

    def __init__(self, config_path:str):
        with open(config_path,'r') as file:
            config = json.load(file)
        self.config = config
        wandb.init(project="outcome-similarity",entity="laiking")
        self.metrics = [load_metric('f1'),load_metric('precision'),load_metric('recall'),load_metric('accuracy')]
        self.exp_name = config_path.split(sep)[-1].split(".")[0]
    
    def compute_metrics(self, results):
        logits, labels = results
        predictions = np.argmax(logits, axis=-1)
        return {metric.name : metric.compute(predictions=predictions, references=labels)[metric.name] for metric in self.metrics}

    def tokenize(self, example):
        tokenized_inputs = self.tokenizer(
            text = example["sentence1"],
            text_pair = example["sentence2"],
            max_length=512,
            truncation=True,
            padding=True,
        )
        return tokenized_inputs


    def prepare_data(self):
        disable_progress_bar()
        datasets = load_from_disk(self.config["dataset_path"])
        self.label_list = datasets["train"].features["label"].names
        self.label2id = {l: i for i, l in enumerate(self.label_list)}
        self.id2label = {i: l for i, l in enumerate(self.label_list)}
        self.tokenizer = AutoTokenizer.from_pretrained(self.config["model_name"], use_fast=True)
        self.tokenized_datasets = datasets.map(self.tokenize,batched=True)
        
    
    def train(self):
        self.model = AutoModelForSequenceClassification.from_pretrained(
            self.config["model_name"],
            num_labels=len(self.label_list),
            id2label=self.id2label,
            label2id=self.label2id,
        )
        eval_steps = 1 + (len(self.tokenized_datasets["train"])*self.config["epochs"]*self.config["evaluation_steps_ratio"]//self.config["batch_size"])

        training_args = TrainingArguments(
            output_dir= join(self.config["train_out_dir"],self.exp_name) ,
            evaluation_strategy="steps",
            eval_steps=eval_steps,
            save_steps=eval_steps,
            load_best_model_at_end=True,
            save_total_limit=1,
            push_to_hub=False,
            disable_tqdm=True,
            report_to="wandb",

            metric_for_best_model="f1",
            greater_is_better=True,
            warmup_ratio=self.config["warmup_ratio"],
            per_device_train_batch_size=self.config["batch_size"],
            per_device_eval_batch_size=self.config["batch_size"],
            learning_rate=self.config["learning_rate"],
            num_train_epochs=self.config["epochs"],
            weight_decay=0.01,
            seed=self.config["seed"],
        )
 
        data_collator = DataCollatorWithPadding(tokenizer=self.tokenizer)
        
        self.trainer = Trainer(
            model=self.model,
            args=training_args,
            train_dataset=self.tokenized_datasets["train"],
            eval_dataset=self.tokenized_datasets["validation"],
            tokenizer=self.tokenizer,
            compute_metrics=self.compute_metrics,
            data_collator=data_collator,
            callbacks=[
                EarlyStoppingCallback(early_stopping_patience=self.config["patience"])
            ]
        )
        self.trainer.train()
    
    def save_test_results(self, save=True):
        results = self.trainer.predict(self.tokenized_datasets["test"])
        if save:
            save_dir = join(self.config["test_out_dir"],self.exp_name)
            try:
                makedirs(save_dir, exist_ok = True)
            except OSError:
                print(f"Directory {save_dir} can not be created")
            with open(f"{save_dir}/metrics.json", "w") as outfile:
                json.dump(results.metrics, outfile, indent=4)
            np.save(f"{save_dir}/predictions.npy", results.predictions)
        return results




if __name__ == "__main__":
    # Argument Parsing 
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", required=True, type=str)
    args = parser.parse_args()

    # Initialization and data loading
    clf = PairClassifier(args.config)
    clf.prepare_data()
    # Training
    clf.train_and_evaluate()
    # Evaluation on test set
    clf.save_test_results()