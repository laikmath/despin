from datasets import Dataset, Value, ClassLabel, Features, Sequence, DatasetDict
from os.path import join
from os import listdir
from typing import List, Tuple, Dict
import json


def load_dataset(filelist, directory):
    sentences1 = []
    sentences2 = []
    labels = []
    sent_ids = []
    for filename in filelist:
        with open(join(directory, filename), 'r', encoding='utf-8') as input_file:
            for l in input_file.readlines()[1:]:
                sent_id, id1, id2, out1, out2, label = l.strip().split('\t')
                sentences1.append(out1)
                sentences2.append(out2)
                labels.append(int(label))
                sent_ids.append(int(sent_id))
    return sent_ids, sentences1, sentences2, labels


def get_full_dataset(sent_ids, sentences1, sentences2, labels):
    """Create Dataset obj from BIO extracted data"""
    examples_dict = {
            'sent_id': sent_ids,
            'sentence1':sentences1,
            'sentence2':sentences2,
            'label': labels,
    }

    features = Features({
        "sent_id" : Value('uint16'),
        "sentence1" : Value('string'),
        "sentence2" : Value('string'),
        "label" : ClassLabel(2,names=["different","similar"]),
    })
    return Dataset.from_dict(examples_dict, features=features)

def split_dataset(dataset:Dataset, seed=42) -> DatasetDict:
    train_test = dataset.train_test_split(0.2, seed=seed)
    test_val = train_test["test"].train_test_split(0.5, seed=seed)
    dataset_dict = DatasetDict({
        "train" : train_test["train"],
        "validation" : test_val["train"],
        "test" : test_val["test"] 
    })
    return dataset_dict


def create_hf_dataset(dataset_dir:str) -> DatasetDict:
    filelist = ["train.tsv"]
    sent_ids, sentences1, sentences2, labels = load_dataset(filelist, dataset_dir)
    full_dataset = get_full_dataset(sent_ids, sentences1, sentences2, labels)
    dataset_dict = split_dataset(full_dataset, seed=42)
    return dataset_dict


if __name__ == "__main__":
    # folder containing primary/reported/primary-secondary outcomes data subfolders
    dataset_dir = "./data/outcome-sim/"
    dataset_dict = create_hf_dataset(dataset_dir)
    dataset_dict.save_to_disk(dataset_dir)