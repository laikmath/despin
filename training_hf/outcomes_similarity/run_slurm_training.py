from typing import Dict, Any, List
from uuid import uuid4
from subprocess import call
from os.path import join, dirname, realpath
from os import makedirs
import time
from typing import Any


def args2str(k: str, v: Any) -> str:
    if isinstance(v, bool):
        if v:
            return f"--{k}"
        return ""
    return f"--{k} {v}"


def generate_batch_file(
        cmd: str, exp_name: str, logdir: str,
        cmd_args: Dict[str, Any], ngpus: int,
        ncpus: int, current_dir:str, preview: bool=False) -> str:
    full_cmd = cmd + " " + " ".join([args2str(k, v) for k, v in cmd_args.items()])
    if preview:
        print("EXP NAME :" , exp_name)
        print(full_cmd,"\n")

    return open(join(current_dir,'template.slurm'), 'r').read().format(
        logdir=logdir, cmd=full_cmd, ngpus=ngpus, ncpus=ncpus, exp_name=exp_name)


def launch_job_list(
        cmd: str, exp_name: str, cmd_args: List[Dict[str, Any]], ngpus: int,
        ncpus: int,current_dir:str, preview: bool=False ) -> None:
    logdir = join('./out/logs', exp_name, time.strftime("%Y_%m_%d_%H_%M_%S"))
    if not preview:
        makedirs(logdir)
    count = 0
    for args in cmd_args:
        slurm_script_fname = join('/tmp', str(uuid4()) + ".slurm")
        slurm_template = generate_batch_file(
            cmd=cmd,
            exp_name=f"{exp_name}_{count}",
            logdir=logdir,
            cmd_args=args,
            ngpus=ngpus,
            ncpus=ncpus,
            preview=preview,
            current_dir=current_dir
        )
        if not preview:
            open(slurm_script_fname, 'w').write(slurm_template)
            call(['sbatch', slurm_script_fname])
        count += 1

if __name__ == '__main__':
    python_file = "train_st_outsim.py"
    exp_name = "sent-medall-mpnet_outcome-sim"

    current_dir = dirname(realpath(__file__))
    config_dir = join(current_dir,"configs")
    arg_list = [{"config":f"./configs/{exp_name}.json","split":i} for i in range(10)]

    cmd = f"python {join(current_dir,python_file)}"
    
    # first preview
    launch_job_list(
        cmd=cmd,
        exp_name=exp_name,
        cmd_args=arg_list,
        current_dir = current_dir,
        ngpus=1,
        ncpus=1,
        preview=False
    )